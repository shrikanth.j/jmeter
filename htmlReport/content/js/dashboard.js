/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 78.78787878787878, "KoPercent": 21.21212121212121};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.5, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.5, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout"], "isController": false}, {"data": [0.25, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-0"], "isController": false}, {"data": [0.5, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-1"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-2"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/subunit"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-3"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-4"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-5"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-6"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/buzz/feed?limit=5&offset=0&sortOrder=DESC&sortField=share.createdAtUtc"], "isController": false}, {"data": [0.625, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/core/i18n/messages"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/locations"], "isController": false}, {"data": [0.5, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-5"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-4"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/action-summary"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-6"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/shortcuts"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-1"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-0"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-3"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-2"], "isController": false}, {"data": [0.5, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-1"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-2"], "isController": false}, {"data": [0.5, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-0"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-5"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-6"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-3"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-4"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/time-at-work?timezoneOffset=5.5&currentDate=2023-01-20&currentTime=16:24"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/leaves?date=2023-01-20"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 66, 14, 21.21212121212121, 2945.954545454546, 269, 24028, 474.0, 12465.900000000049, 21656.85, 24028.0, 1.4941591958706875, 383.5523281150956, 1.3389155601965046], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout", 2, 0, 0.0, 1404.0, 1349, 1459, 1404.0, 1459.0, 1459.0, 1459.0, 0.7468259895444362, 7.994101241598208, 3.3154989731142646], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-0", 2, 0, 0.0, 1533.0, 1279, 1787, 1533.0, 1787.0, 1787.0, 1787.0, 1.1191941801902632, 1.9247079602686066, 0.8361167459429212], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-1", 2, 0, 0.0, 539.0, 535, 543, 539.0, 543.0, 543.0, 543.0, 3.6832412523020257, 16.051234461325965, 2.2516689686924494], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-2", 2, 0, 0.0, 270.5, 269, 272, 270.5, 272.0, 272.0, 272.0, 7.352941176470588, 12.020335477941176, 4.566865808823529], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/subunit", 2, 2, 100.0, 475.5, 440, 511, 475.5, 511.0, 511.0, 511.0, 2.7137042062415193, 0.8692333785617368, 1.4072040366350067], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate", 2, 0, 0.0, 23721.5, 23415, 24028, 23721.5, 24028.0, 24028.0, 24028.0, 0.083236224404861, 350.5929686849717, 0.3728560169385717], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-3", 2, 0, 0.0, 7820.5, 7799, 7842, 7820.5, 7842.0, 7842.0, 7842.0, 0.2550369803621525, 140.17644374840603, 0.16089247003315482], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-4", 2, 0, 0.0, 8967.5, 8962, 8973, 8967.5, 8973.0, 8973.0, 8973.0, 0.22289089490694303, 145.0856475677031, 0.13843614175860916], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-5", 2, 0, 0.0, 21640.5, 21586, 21695, 21640.5, 21695.0, 21695.0, 21695.0, 0.09217014608968155, 141.31510596686482, 0.05796638093921378], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-6", 2, 0, 0.0, 20663.0, 20616, 20710, 20663.0, 20710.0, 20710.0, 20710.0, 0.09657170449058426, 142.01397271849348, 0.05979146547561564], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/buzz/feed?limit=5&offset=0&sortOrder=DESC&sortField=share.createdAtUtc", 2, 2, 100.0, 483.5, 480, 487, 483.5, 487.0, 487.0, 487.0, 3.1695721077654517, 1.0152535657686212, 1.5847860538827259], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/core/i18n/messages", 4, 0, 0.0, 514.75, 457, 545, 528.5, 545.0, 545.0, 545.0, 0.19747235387045814, 0.20287198854660346, 0.10220737065560821], "isController": false}, {"data": ["Test", 2, 2, 100.0, 30681.5, 30261, 31102, 30681.5, 31102.0, 31102.0, 31102.0, 0.06430454633142563, 272.50445610410907, 1.1027852911388336], "isController": true}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/locations", 2, 2, 100.0, 450.5, 445, 456, 450.5, 456.0, 456.0, 456.0, 2.6490066225165565, 0.8485099337748344, 1.3788286423841059], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push", 2, 0, 0.0, 1233.5, 1231, 1236, 1233.5, 1236.0, 1236.0, 1236.0, 1.29366106080207, 13.831687621280723, 4.531603735446313], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-5", 2, 0, 0.0, 302.0, 297, 307, 302.0, 307.0, 307.0, 307.0, 3.2679738562091503, 3.172232434640523, 1.7073886846405228], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-4", 2, 0, 0.0, 309.5, 308, 311, 309.5, 311.0, 311.0, 311.0, 3.1948881789137378, 3.098167931309904, 1.6411242012779552], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/action-summary", 2, 2, 100.0, 505.0, 504, 506, 505.0, 506.0, 506.0, 506.0, 3.3112582781456954, 1.060637417218543, 1.7397040562913908], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-6", 2, 0, 0.0, 304.5, 302, 307, 304.5, 307.0, 307.0, 307.0, 3.2414910858995136, 3.1465255267423013, 1.6618972852512155], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/shortcuts", 2, 2, 100.0, 459.0, 438, 480, 459.0, 480.0, 480.0, 480.0, 3.442340791738382, 1.1026247848537005, 1.7581486660929433], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-1", 2, 0, 0.0, 474.0, 473, 475, 474.0, 475.0, 475.0, 475.0, 2.5380710659898473, 10.718640942258883, 1.060834390862944], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-0", 2, 0, 0.0, 439.5, 439, 440, 439.5, 440.0, 440.0, 440.0, 2.6525198938992043, 4.297393070291777, 1.328850298408488], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-3", 2, 0, 0.0, 309.0, 298, 320, 309.0, 320.0, 320.0, 320.0, 3.2626427406199023, 3.1638713295269167, 1.70778955954323], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-2", 2, 0, 0.0, 311.5, 308, 315, 311.5, 315.0, 315.0, 315.0, 3.1746031746031744, 3.072296626984127, 1.6245039682539681], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-1", 2, 0, 0.0, 547.0, 505, 589, 547.0, 589.0, 589.0, 589.0, 1.1098779134295227, 4.700723154827969, 0.62539019145394], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-2", 2, 0, 0.0, 316.0, 310, 322, 316.0, 322.0, 322.0, 322.0, 1.3131976362442546, 1.270877790544977, 0.863068368351937], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-0", 2, 0, 0.0, 531.0, 515, 547, 531.0, 547.0, 547.0, 547.0, 1.0845986984815619, 1.7571769929501084, 0.6122051247288502], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-5", 2, 0, 0.0, 309.5, 305, 314, 309.5, 314.0, 314.0, 314.0, 1.309757694826457, 1.2713858873608384, 0.8748772102161101], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-6", 2, 0, 0.0, 313.0, 308, 318, 313.0, 318.0, 318.0, 318.0, 1.3063357282821686, 1.268064173742652, 0.8598342586544743], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-3", 2, 0, 0.0, 323.0, 317, 329, 323.0, 329.0, 329.0, 329.0, 1.297016861219196, 1.2577517023346303, 0.8676333495460441], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-4", 2, 0, 0.0, 311.5, 305, 318, 311.5, 318.0, 318.0, 318.0, 1.3063357282821686, 1.2667884552580013, 0.8611099771391248], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/time-at-work?timezoneOffset=5.5&currentDate=2023-01-20&currentTime=16:24", 2, 2, 100.0, 443.0, 440, 446, 443.0, 446.0, 446.0, 446.0, 3.676470588235294, 1.1776194852941175, 2.1470013786764706], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/leaves?date=2023-01-20", 2, 2, 100.0, 476.5, 440, 513, 476.5, 513.0, 513.0, 513.0, 3.003003003003003, 0.9618993993993994, 1.6012105855855856], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["401/Unauthorized", 14, 100.0, 21.21212121212121], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 66, 14, "401/Unauthorized", 14, "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/subunit", 2, 2, "401/Unauthorized", 2, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/buzz/feed?limit=5&offset=0&sortOrder=DESC&sortField=share.createdAtUtc", 2, 2, "401/Unauthorized", 2, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/locations", 2, 2, "401/Unauthorized", 2, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/action-summary", 2, 2, "401/Unauthorized", 2, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/shortcuts", 2, 2, "401/Unauthorized", 2, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/time-at-work?timezoneOffset=5.5&currentDate=2023-01-20&currentTime=16:24", 2, 2, "401/Unauthorized", 2, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/leaves?date=2023-01-20", 2, 2, "401/Unauthorized", 2, "", "", "", "", "", "", "", ""], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
